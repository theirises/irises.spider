import requests
import aiohttp #支持异步的网络请求模块，requests不支持异步方法
import asyncio
import time

# 普通下载函数
def download_image(url):
    print('开始下载',url)
    # 发送请求，下载图片
    res = requests.get(url)
    print('下载完成')
    # 保存至本地
    file_name = url.rsplit('_')[-1]
    with open(file_name, mode='wb') as file_object:
        file_object.write(res.content)

def commond_download(url_list):
    for item in url_list:
        download_image(item)


# 协程下载函数
async def fetch(session, url):
    print('发送请求',url)
    async with session.get(url, verify_ssl=False) as res:
        content = await res.content.read()
        file_name = url.rsplit('_')[-1]
        with open(file_name, mode='wb') as file_object:
            file_object.write(content)
        print('下载完成')

async def asyncio_download(url_list):
    async with aiohttp.ClientSession() as session:
        tasks = [asyncio.create_task(fetch(session, url)) for url in url_list]
        await asyncio.wait(tasks)
        
if __name__ == '__main__':
    url_list = [
        'http://car3.autoimg.cn/cardfs/selected/520/g24/M08/E0/62/520x390_0_q95_autohomecar__Chtk3WDVs3KAKJg_AAKylpHqqzE068.jpg',
        'http://car3.autoimg.cn/cardfs/selected/520/g8/M05/B0/20/520x390_0_q95_autohomecar__ChwEmmDq4-yAQHpUAAIuFOx_Z14142.jpg', 	
        'http://car3.autoimg.cn/cardfs/selected/520/g24/M08/AD/40/520x390_0_q95_autohomecar__ChwFjmDP9fqAccZRAAGtWXyLtSI293.jpg'
    ]
    start = time.time()
    # commond_download(url_list)    #普通下载
    asyncio.run(asyncio_download(url_list))    #协程下载
    end = time.time()
    print("下载所需时间:%.2f秒"%(end-start))
import asyncio
import requests

async def download_image(url):
    # 发送网络请求，下载图片（遇到网络下载图片的IO请求，自动化切换到其他任务）
    print('开始下载',url)

    loop = asyncio.get_event_loop()
    # requests模块默认不支持异步操作，所以就使用线程池来配合实现了。
    future = loop.run_in_executor(None, requests.get, url)

    response = await future
    print('下载完成')
    # 图片保存到本地文件
    file_name = url.rsplit('_')[-1]
    with open(file_name, mode='wb') as file_object:
        file_object.write(response.content)

if __name__ == '__main__':
    url_list = [
        'http://car3.autoimg.cn/cardfs/selected/520/g24/M08/E0/62/520x390_0_q95_autohomecar__Chtk3WDVs3KAKJg_AAKylpHqqzE068.jpg',
        'http://car3.autoimg.cn/cardfs/selected/520/g8/M05/B0/20/520x390_0_q95_autohomecar__ChwEmmDq4-yAQHpUAAIuFOx_Z14142.jpg', 	
        'http://car3.autoimg.cn/cardfs/selected/520/g24/M08/AD/40/520x390_0_q95_autohomecar__ChwFjmDP9fqAccZRAAGtWXyLtSI293.jpg'
    ]
    tasks = [ download_image(url) for url in url_list]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait(tasks))



import requests
from lxml import etree
import random
import json
import re
import time

# 导入线程池模块对应的类
from multiprocessing.dummy import Pool

# 写一个下载视频的函数用于多线程池----先看下面
def download(url):
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Safari/537.36'
    }
    print('开始下载')
    data = requests.get(url=url, headers=headers).content
    # 其实之前网页上有名字，但是忘了保存这个数据...我就用随机数代替了...

    # 视频数据会以二进制格式返回，可以直接写入，图片同理
    with open(str(random.randint(1,9999))+'.mp4', 'wb') as fp:
        fp.write(data)
    print('下载完毕')           #对于漫长的过程，在某些时刻给予一定回应是必要的

video_urls = []


if __name__ == "__main__":

    # 存放所有视频链接的列表
    video_urls = []


    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36 Edg/91.0.864.71'
    }

    # 对下述url发起请求，解析出视频详情页的url和视频名称
    url = 'https://www.pearvideo.com/category_135'
    res = requests.get(url=url,headers=headers)
    html = etree.HTML(res.content)
    li_list = html.xpath("//ul[@id='listvideoListUl']/li")
    for li in li_list:
        detail_url = 'https://www.pearvideo.com/' + li.xpath('./div/a/@href')[0]
        name = li.xpath('./div/a/div[2]/text()')[0]
        #对详情页发起请求
        detail_page = requests.get(url=url,headers=headers).text
        #从详情页中解析出视频的地址(url)
        #当前页面的视频数据是通过ajax请求获取的,ajax返回的数据中包含其视频url
        #我当前捕获的该ajax请求url为https://www.pearvideo.com/videoStatus.jsp?contId=1736095&mrd=0.3081195743189289
        #可以看到有两个参数，一个为contId，其实就是这个视频的id，也就是当前视频详情页yrl后半部分，另一个为mrd，通过全局搜索，发现其通过math.random()函数生成，至此获得了该ajax请求方式

        # 1.获得contId
        contId = detail_url.split('/')[-1].split('_')[-1]
        # print(contId)
        # 2.拼接获得完整ajax请求url
        ajax_url = 'https://www.pearvideo.com/videoStatus.jsp'
        # print(ajax_url)
        params = {
            'contId':contId,
            'mrd':str(random.random())
        }
        ajax_headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36 Edg/91.0.864.71',
            'Cookie': '__secdyid=297275ce08e92a152d150ca2d11633c127b72f12e621266b021627058712; JSESSIONID=3CB56197F2DEE32C2A076DB2FD495362; PEAR_UUID=a88f9977-22d0-405d-a1fc-21862529bc8f; _uab_collina=162705871310554083804245; UM_distinctid=17ad441b52570c-0a0fc862d0278f-7a697f6c-144000-17ad441b5265fd; Hm_lvt_9707bc8d5f6bba210e7218b8496f076a=1627058714; acw_tc=781bad2816270614448602522e22e8b290eb53b9912ec37dc698026976511b; SERVERID=bacac21aafa9027952fdc46518c0c74f|1627061445|1627058712; p_h5_u=E0238D88-608A-4756-9BC3-F5BC6FDD4674; Hm_lpvt_9707bc8d5f6bba210e7218b8496f076a=1627061446; CNZZDATA1260553744=975462545-1627055069-https%253A%252F%252Fwww.baidu.com%252F%7C1627060499',
            'referer': 'https://www.pearvideo.com/video_'+ contId
        }
        # 请求头尽量写全...像这个网站如果只写一个user-agent返回的数据永远是视频已下架...我们要模仿的像人一点
        vidio_res = requests.get(url=ajax_url,headers=ajax_headers,params=params).text
        # print(vidio_res)

        # 这时候可以看到返回的数据是json格式的，其中包含了视频的url,我们使用json对其进行解析
        obj = json.loads(vidio_res)
        video_url = obj['videoInfo']['videos']['srcUrl']
        # print(video_url)
        # 这里有个坑，它虽然返回了Url，但这个Url不是完全正确的，比对这个视频元素标签上的url，发现有一点点不一样
        # https://video.pearvideo.com/mp4/adshort/20210723/cont-1736138-15727009_adpkg-ad_hd.mp4 这是视频标签上的url
        # https://video.pearvideo.com/mp4/adshort/20210723/1627064782620-15727805_adpkg-ad_hd.mp4 这是咱返回的url
        # 对其进行处理
        new_video_url = re.sub(r'(?<=/)(\d+)(?=-)', 'cont-'+str(contId), video_url)
        # print(new_video_url)
        # 这里使用了正则替换，当然用split,replace这些可能简单一点，但是比较繁琐
        video_urls.append(new_video_url)
        # 把视频链接全放进这个全局变量里，为了后续使用多线程进行分布式爬取

    # 此时有了所有的链接，然后编写一个下载视频的函数，在上面

    # 为了提高效率，引入多线程，因为下载视频可能需要比较久，容易造成阻塞
    # 实例化一个线程池对象
    pool = Pool(4)      #包含四个线程，效率可以提升近四倍（非饱和状态下），当然带宽、cpu等资源消耗也增加了四倍
    pool.map(download,video_urls)     #参数1为方法，参数2为可迭代对象，返回值为一个列表




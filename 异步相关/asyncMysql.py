import asyncio
import aiomysql

'''
    异步链接数据库
    数据库链接、操作、关闭都需要网络IO操作
    其他数据库需要查询对应数据库的异步模块,并查看官方文档
    若没有相关模块，则需要异步与非异步混合的方法，即 future = loop.run_in_executor(None, func, params)
'''

async def execute(host,password):
    # 网络IO操作:连接Mysql
    conn = await aiomysql.connect(host='127.0.0.1', port=3306, user='root',password='123',db='mysql')

    # 网络IO操作:创建cursor
    cur = await conn.cursor()

    # 网络IO操作:执行sql
    await cur.execute('SELECT * FROM user')

    # 网络IO操作:获取SQL结果
    result = await cur.fetchall()

    # 网络IO操作:关闭连接
    await cur.close()
    conn.close()
    print('结束',host)


task_list = [
    execute('47.93.41.197','123'),
    execute('47.93.42.197','123'),
]

asyncio.run(asyncio.wait(task_list))



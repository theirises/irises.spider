import requests
from bs4 import BeautifulSoup
import lxml
import re
import time

# 模拟登录人人网

url = 'http://www.renren.com/login'
params = {
    'to': 'http://www.renren.com/PLogin.do'
}

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55'
}

''' 登陆参数
    appKey: "bcceb522717c2c49f895b561fa913d10"      未知参数需要查找,其实这玩意每次都相同...也算固定的
    callId: "1627125127761"                         未知参数，进行查找判断
    password: "*******"    加密后的密码，一般固定
    sessionKey: ""                                  空参数
    sig: "976d398ad1cec38e1ff9845e6be4c51f"         未知参数
    user: "********"                             用户名
'''


res = requests.get(url=url,headers=headers,params=params).text
soup = BeautifulSoup(res,'html.parser')
# 网页中的响应数据，发现第一个参数在javascript的代码中，通过beautifulsoup与正则进行提取
pattern = re.compile(r'(?<="secretKey":")([a-z0-9]+)(?=","sessionKey)')
js = soup.find('script',text=pattern)   #这里其实不是很明白它返回的为什么是匹配成功的包含该内容的soup对象，而不是直接提取出我所匹配的部分
appKey = re.findall(pattern,js.text)[0]
# print(appKey)


#   第二个参数callId在编译后的js文件中可以看到定义为 (new Date).getTime().toString()
#   因此应该是个时间戳,经查证是毫秒级时间戳函数
time = int(round(time.time()*1000))
# print(time)

data = {
    'appKey': appKey,
    'callId': time,
    'password': "*********",
    'sessionKey': "",
    'sig': "976d398ad1cec38e1ff9845e6be4c51f",    # 对于暂时无法破解的，可以先保持不变发送请求尝试
    'user': "*******"      
}

login_url = 'http://rrwapi.renren.com/account/v1/loginByPassword'

login_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55',
    'Referer': 'http://www.renren.com/',
    'Origin': 'http://www.renren.com',
    'Host': 'rrwapi.renren.com',
    'Content-Type': 'application/json;charset=UTF-8',
    'Content-Length': '194',
    'Connection': 'keep-alive',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'Accept-Encoding': 'gzip, deflate',
    'Accept': 'application/json, text/plain, */*'
}

import requests
from bs4 import BeautifulSoup

url='https://movie.douban.com/top250'
headers ={
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55'
}

res = requests.get(url=url,headers=headers)
soup = BeautifulSoup(res.text, "html.parser")

article = soup.find(class_='article')
for movie in article.findAll(class_='item'):
    title = movie.select('span[class="title"]')[0].string
    try:
        rating = movie.select('span[class="rating_num"]')[0].string
    except IndexError:
        rating = '无评分'
    try:
        comments = movie.select('span[class="inq"]')[0].string
    except IndexError:
        comments = '无评论'
    print(title,rating,comments)
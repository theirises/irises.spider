import requests
from bs4 import BeautifulSoup
from lxml import etree

for i in range(1):
    url = 'https://movie.douban.com/top250?start=' + str(i*25) + '&filter='
    headers={
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55',
        'Cookie':'bid=2vJOA6iqA1Y; douban-fav-remind=1; __gads=ID=a05c326d0e02dc04-2288b64855c60002:T=1615464191:RT=1615464191:S=ALNI_MYyQTSqkmGdZLNKJWuGHv_ClNjsvA; ll="118172"; viewed="34938311"; gr_user_id=d16335b6-9030-4b50-8314-63a86ae131e9; __utmc=30149280; __utmc=223695111; dbcl2="243174253:MjQBL0dJ9SY"; ck=71Nt; ap_v=0,6.0; push_noty_num=0; push_doumail_num=0; _pk_ref.100001.4cf6=%5B%22%22%2C%22%22%2C1627664256%2C%22https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DfMtYEKujXExXZPgM3ATTBTPb9Ro0Kdu4kRXiRRgsRl_AX8lAnX9kDvGe-VEtQ9L0%26wd%3D%26eqid%3Dc63a183400000226000000056104003a%22%5D; _pk_ses.100001.4cf6=*; _pk_id.100001.4cf6=981a1cd9ee6cba7a.1625412306.5.1627666108.1627652157.; __utma=30149280.1252322629.1624284672.1627652158.1627666108.8; __utmb=30149280.0.10.1627666108; __utmz=30149280.1627666108.8.7.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; __utma=223695111.1313755729.1625412308.1627652158.1627666108.5; __utmb=223695111.0.10.1627666108; __utmz=223695111.1627666108.5.4.utmcsr=baidu|utmccn=(organic)|utmcmd=organic',
        'Host':'movie.douban.com'
    }

    res = requests.get(url=url,headers=headers)

    # 方法一 Beautifulsoup   通过.text输出该标签下所有文本
    # soup = BeautifulSoup(res.text, "html.parser")
    # article = soup.find(class_='article')
    # for li in article.findAll('li'):
    #     # name = li.select('.title')[0].string
    #     bd = li.select('.bd')[0]
    #     author = bd.select('p')[0]
    #     print(author.text)









    # 方法二 Beautifulsoup   通过.replace,在解析之前去掉多余标签
    # soup = BeautifulSoup(res.text.replace('<br>',''), "html.parser")
    # article = soup.find(class_='article')
    # for li in article.findAll('li'):
    #     # name = li.select('.title')[0].string
    #     bd = li.select('.bd')[0]
    #     author = bd.select('p')[0]
    #     print(author.string)







    # 方法二 xpath  注意路径连续，注意不需要循环遍历节点
    # html = etree.HTML(res.text)
    # author = html.xpath("//div[@class='article']/ol/li/div/div[@class='info']/div[@class='bd']/p[1]/text()")
    # author = html.xpath('//*[@id="content"]/div/div[1]/ol/li[1]/div/div[2]/div[2]/p[1]/text()[1]')
    # author = html.xpath('//div[@class="info"]/div[@class="bd"]/p[1]/text()')

    # xpath 不需要循环遍历
    # li = html.xpath("//div[@class='article']/ol/li")
    # for i in li:
    #     author = i.xpath('//div[@class="bd"]/p[1]/text()')
    #     print(author)

    '''
        导演: 弗兰克·德拉邦特 Frank Darabont   主演: 蒂姆·罗宾斯 Tim Robbins /...
        遍历后得到
        25*25
        25个长度为25的列表
    '''

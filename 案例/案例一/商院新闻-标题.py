import requests
from bs4 import BeautifulSoup
import re


headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55'
}
def get_page(url):
    response = requests.get(url=url, headers=headers)
    response.encoding=response.apparent_encoding
    # print(response.text)
    soup = BeautifulSoup(response.text,'html.parser')
    page_element = soup.select('td[id=fanye44467]')
    # print(page_element)
    page = re.findall('/(\d+)',page_element[0].text)
    page_count = page[0]
    return page_count

def get_all_url(page):
    url_list = ['http://nbubs.nbu.edu.cn/sydt/xyxw.htm']
    for i in range(int(page)-1,0,-1):
        url_list = url_list + ['http://nbubs.nbu.edu.cn/sydt/xyxw/'+ str(i) +'.htm']
    return url_list

def get_title(url):
    res = requests.get(url,headers=headers)
    res.encoding=res.apparent_encoding
    soup = BeautifulSoup(res.text,"html.parser")
    title = soup.findAll(class_='c44467')
    for i in title:
        print(i.string)

if __name__ == '__main__':
    page_count = get_page('http://nbubs.nbu.edu.cn/sydt/xyxw.htm')
    url_list = get_all_url(page_count)
    for url in url_list:
        get_title(url)
import requests
from lxml import etree
from ua_c import ua_list
import random


url = 'https://search.51job.com/list/000000,000000,0000,00,9,99,+,2,1.html?lang=c&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=99&companysize=99&ord_field=0&dibiaoid=0&line=&welfare='
ua = ua_list['chrome'] # 使用模拟谷歌浏览器的UA

'''
    url参数解析
    关于url地址中000000,000000,0000,00,9,99,+,2,1这一段
        首个000000 表示城市为所有，若北京则为010000，若上海则为020000，若多个城市合并查询，则用%252c拼接，例如上海加北京为010000%252c020000%252c030200
        第二个000000 表示地区地标，若朝阳区则为010500
        0000 表示工作职能为所有，若0121则表示java开发工程师，同样多个则用%252c拼接
        00 表示行业领域为所有，若01则表示计算机软件，同样多个则用%252c拼接
        99 表示薪水范围为所有，99表所有，若01表2000以下，若自定义，例如2000-3000，则为使用2000-3000代替99/01
        9 表示发布日期为所有，若0则表示24小时之内，若1则表示近三天
        + 表示搜索框内没有搜索全文/公司，若为%25E4%25BA%25BA%25E4%25BA%258B则表示搜索了人事，暂时不知是何编码格式；若有排除关键字，相当于在搜索框中输入“-关键字”
        2 暂时未知
        1 表示当前数据页数
    其余url参数
        lang: c
        postchannel: 0000   未知，从命名来看，可能是手机与PC访问的区别
        workyear: 99        表示工作年限
        cotype: 99          表示公司性质
        degreefrom: 99      表示学历要求
        jobterm: 99         表示工作类型 99-所有 01全职
        companysize: 99     表示公司规模
        ord_field: 0        暂时未知
        dibiaoid: 0         表示地标，若为热门地区则为01，若中关村则为38
        line:               暂时未知
        welfare:            表示公司福利，若为空则无该筛选项
    请根据需要自行拼接url
'''

def getData(url):
    headers = {
        'user-agent': random.choice(ua),
        'Accept': 'application/json, text/javascript, */*; q=0.01'  # 这是必要的，这个请求头将标记以接口形式返回json数据
    }
    res = requests.get(url=url,headers=headers).text
    return res

print(getData(url))


'''
    抓取数据部分就只需要这几行，由于是json数据接口的形式，提取数据也很简单
    后续可能需要提高高并发的效率（多线程/进程、协程异步、分布式等等），同时避免被检查异常（IP代理池、控制效率、错误异常控制等）
'''


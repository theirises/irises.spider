const http = require('http')
const fs = require('fs')
const { json } = require('express')


const server = http.createServer((req, res) => {
    if (req.method === 'GET') {
        fs.readFile('./content/content.txt', {flag: 'r+', encoding: 'utf8'}, (err, data) => {
            if(err) {
                res.writeHead(404, "not found")
                res.end("<h1>404 NOT FOUND<h1>")
             return;
            }
            const dataList = data.split(/\n/)
            let html = ''
            dataList.forEach(row => {
                let tr = ''
                if (!row) {
                    return
                }
                row = JSON.parse(row)
                for (const key in row) {
                    tr += `<td>${row[key]}</td>`
                }
                tr = '<tr>' + tr +'</tr>'
                html += tr
            })
            html = '<table><tr><th>名称</th><th>日期</th><th>最新价</th><th>最高价</th><th>最低价</th><th>涨跌额</th><th>涨跌幅</th><th>时间</th><tr>' + html + '</table>'
            res.writeHead(200,{'Content-Type':'text/html;charset=UTF8'})
            res.end(html)
        })
    }
})
server.listen(8000)


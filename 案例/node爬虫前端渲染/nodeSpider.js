const superagent = require('superagent')
const cheerio = require('cheerio')
const moment = require('moment')
const fs = require('fs')


getData()

function getData() {
    superagent
    .get('https://cn.investing.com/indices/indices-futures')
    .set('user-agent','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36')
    .set('cookie','udid=88a437e5d2c43d9b88e336d98cbe177d; __cflb=0H28uxmf5JNxjDUC5huTKPtuPTatsefBtCYyvAbt3Wt; _gid=GA1.2.586645209.1631520640; __gads=ID=319faa476ed8d15a:T=1631520463:S=ALNI_MZ0sroxCrm4hS3wPuszv5IyH8bLEA; smd=c8be22872901489d96fd60b979fa2265-1631536248; cf_clearance=YG18Hnnq1aClx7UmBuUbfCGdMrT3ucVE9iJg66msYpU-1631536402-0-150; cf_chl_2=1777f7120b905f0; cf_chl_prog=a9; _dd_s=logs=1&id=07944a90-6b76-4d1c-96df-41b2a4799d95&created=1631536352635&expire=1631537252635; _gat_UA-2555300-55=1; _ga_H1WYEJQ780=GS1.1.1631536248.3.1.1631536353.0; _ga=GA1.1.1897329000.1631520639; __cf_bm=lw6F_aLvj5BanLPxQgqO4X3Kf5WcrBIZuqrEZYcZCgk-1631536493-0-AUEwC0RXzro7biupI7ff5Ha5l5LFejtVlNf3e0qxUyQbcYK9SNfm1GX4xGTOXe9ZJCxsr8rnxUrqF+84h2uM9ymwZ2HDQFMvUq/ceOzE8M0441WpzgyHP7OoIrRdORVWDw==')
    .end((err,res) => {
        if (err) {
            console.log(err.status)
            return
        }
        let $ = cheerio.load(res.text)
        //抓取需要的数据,each为cheerio提供的方法用来遍历
        let content = ''
        $('main .datatable_row__2vgJl').each( (index,element) => {
            if (index === 0) {
                return
            }
            let temp = {
                'name':$($(element).find('td a')[0]).text() || '',
                'date':$($(element).find('td')[2]).text().replace(/Ex./g,'') || '',
                'new':$($(element).find('td')[3]).text() || '',
                'hight':$($(element).find('td')[4]).text() || '',
                'low':$($(element).find('td')[5]).text() || '',
                'change':$($(element).find('td')[6]).text() || '',
                'rate':$($(element).find('td')[7]).text() || '',
                'time':$($(element).find('td')[8]).text() || '',
            }
            content += JSON.stringify(temp) + '\n'
            // console.log(temp)
        })
        console.log('数据抓取成功')
        console.log(moment().format('YYYY-MM-DDTHH:mm:ss'))
        fs.writeFile('./content/content.txt',content.toString(),(err)=>{
            console.log(err)
        });
        setTimeout(() => getData(), 1000 * 60 * 5)
        // const filename = moment().format('YYYY-MM-DDTHH:mm:ss')
        // fs.writeFile(`${filename}.txt`,content.toString(),(err)=>{
        //     console.log(err)
        // });
    })
}
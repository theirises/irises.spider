import requests
from bs4 import BeautifulSoup
import time
import random
import xlwt


if __name__=='__main__':
    base_url = 'https://wuhan.anjuke.com/sale/p'
    userAgent = [
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50',
        'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1',
        'Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1',
        'Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11',
        'Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.11',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SE 2.X MetaSr 1.0; SE 2.X MetaSr 1.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)'
    ]
    headers = {
        'cookie': 'aQQ_ajkguid=EFA46A42-6125-4460-84C5-0A5C9E0067F9; sessid=C6EBEA1E-591C-4600-8609-A599851FFCBF; ajk-appVersion=; ctid=22; fzq_h=0762969cb624b57baaa5ea63e2f72dea_1628517136762_e4d3d96e335d4ef082b60e2789953e5f_47924990247706939979350991816674799397; id58=e87rkGERMxOLs3L+E6mMAg==; twe=2; 58tj_uuid=a7792de8-2adb-41fa-9cd1-161fbc3b08f6; _ga=GA1.2.1161685456.1628517151; _gid=GA1.2.2009582670.1628517151; als=0; new_session=1; init_refer=; new_uv=2; _gat=1; fzq_js_anjuke_ershoufang_pc=aa56824e88ce2f42ec220acfcf5aaa6f_1628521032060_23; obtain_by=1; xxzl_cid=a6cbf16affd745b0aec58929bad6993b; xzuid=79241320-1013-4014-be8b-c88d0143e3f0',
        'user-agent': userAgent[random.randint(0,len(userAgent))]
    }
    workbook = xlwt.Workbook(encoding='utf-8', style_compression=0)
    sheet = workbook.add_sheet('anjuke', cell_overwrite_ok=True)
    for i in range(20):
        url = base_url+str(i)+'/'
        res = requests.get(url=url,headers=headers)
        soup = BeautifulSoup(res.content,'lxml')
        li = soup.find('section',class_='list')
        count = 0
        for item in li.findAll('div',class_='property-content'):
            try:
                title = item.find('h3').string.replace(' ','').replace('\n','')
            except:
                title = ''
            try:
                style = item.findAll('p',class_='property-content-info-text')[0].text.replace(' ','').replace('\n','')
            except:
                style = ''
            try:
                size = item.findAll('p',class_='property-content-info-text')[1].string.replace(' ','').replace('\n','')
            except:
                size = ''
            try:
                forward = item.findAll('p',class_='property-content-info-text')[2].string.replace(' ','').replace('\n','')
            except:
                forward = ''
            try:
                floor = item.findAll('p',class_='property-content-info-text')[3].string.replace(' ','').replace('\n','')
            except:
                floor = ''
            try:
                createtime = item.findAll('p',class_='property-content-info-text')[4].string.replace(' ','').replace('\n','')
            except:
                createtime = ''
            try:
                estate = item.find('p',class_='property-content-info-comm-name').string.replace(' ','').replace('\n','')
            except:
                estate = ''
            try:
                address = item.find('p',class_='property-content-info-comm-address').text.replace(' ','').replace('\n','')
            except:
                address = ''
            try:
                comment = item.find('div',class_='property-extra').text.replace(' ','').replace('\n','')
            except:
                comment = ''
            try:
                total_price = item.find('p',class_='property-price-total').text.replace(' ','').replace('\n','')
            except:
                total_price = ''
            try:
                average_price = item.find('p',class_='property-price-average').string.replace(' ','').replace('\n','')
            except:
                average_price = ''
            sheet.write(38*i+count, 0, title)
            sheet.write(38*i+count, 1, style)
            sheet.write(38*i+count, 2, size)
            sheet.write(38*i+count, 3, forward)
            sheet.write(38*i+count, 4, floor)
            sheet.write(38*i+count, 5, createtime)
            sheet.write(38*i+count, 6, estate)
            sheet.write(38*i+count, 7, address)
            sheet.write(38*i+count, 8, comment)
            sheet.write(38*i+count, 9, total_price)
            sheet.write(38*i+count, 10, average_price)
            count += 1
        print('第'+str(i)+'页已抓取完毕')
        time.sleep(random.randint(10,20))
    workbook.save(r'./anjuke.xls')




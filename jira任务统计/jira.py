
from asyncore import write
import requests
from bs4 import BeautifulSoup
import time
import json
import os
import matplotlib.pyplot as plt
import xlwt

def getData():
    LOGIN_URL = PREFIXES + '/login.jsp'
    HEADERS = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Connection': 'keep-alive',
        'Host': '172.20.200.191:8080',
        'Pragma': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
        # 'Cookie': 'JSESSIONID=E1486D8CECC88763D8615C55AD403082; atlassian.xsrf.token=BMF8-JWSP-9O18-MSG7_336da3f28209f65ef0c9604ddc8df7a8438a19f0_lin;'
    }

    login_params = {
        'os_username': 'admin',
        'os_password': '**********.',
        'os_destination': '',
        'user_role': '',
        'atl_token': '',
        'login': '登录'
    }

    session = requests.Session()
    # 模拟登录
    login_res = session.post(url=LOGIN_URL, data=login_params, headers=HEADERS)
    # 获取所有版本信息
    VERSIONS_URL = PREFIXES + '/rest/projects/1.0/project/B2BSM/release/allversions-nodetails?_=' + str(int(round(time.time() * 1000)))
    total_version_data = session.get(url=VERSIONS_URL).json()
    # print(total_version_data)

    statistic_version = None

    # 获取目标版本url
    for version_obj in total_version_data:
        if version in version_obj['name']:
            statistic_version = version_obj

    print('统计版本：', statistic_version['name'])

    # 获取版本任务列表
    ISSUE_LIST_URL = PREFIXES + '/projects/B2BSM/versions/' + statistic_version['id']
    issue_list_page = session.get(url=ISSUE_LIST_URL)
    issue_list_soup = BeautifulSoup(issue_list_page.text, "lxml")
    issue_lists = issue_list_soup.find_all('a', class_='issue-key')
    print('当前版本任务总数:', len(issue_lists))

    detail_data = {}
    index = 1
    for issue in issue_lists[:]:
        print('第{0}个 开始采集: {1}'.format(index, issue.text))
        detail_url = PREFIXES + '/rest/api/2/issue/' + issue.text + '?expand=changelog&fields=*all'
        detail_res = session.get(url=detail_url).json()
        key = detail_res['key']
        detail_data[key] = {
            "summary": detail_res['fields']['summary'],
            "issuetype": detail_res['fields']['issuetype']['name'],
            "assignee": detail_res['fields']['assignee']['displayName'], # 经办人
            "reporter": detail_res['fields']['reporter']['displayName'],
            "tester": '',
            "issuelinks": [],
            "create_time": detail_res['fields']['created'],
            'respondent_time': '', # sub-bug统计指标——响应时间
            "test_start_time": '',
            "resolve_time": '',
            "comments": []
        }

        # 工作量记录
        try:
            detail_data[key]['workload'] = detail_res['fields']['timetracking']['originalEstimateSeconds']
        except:
            detail_data[key]['workload'] = 0

        # sub-bug记录父节点
        if (detail_res['fields']['issuetype']['name']) == 'sub-Bug':
            parent = detail_res['fields']['parent']
            detail_data[key]['parent'] = {
                'key': parent['key'],
                'summary': parent['fields']['summary']
            }

        # 关联任务
        issuelinks = detail_res['fields']['issuelinks']
        for issue in issuelinks:
            if 'outwardIssue' in issue:
                issue_obj = {
                    "key": issue['outwardIssue']['key'],
                    "summary": issue['outwardIssue']['fields']["summary"],
                    "issuetype": issue['outwardIssue']['fields']["issuetype"]["name"],
                    "linktype": '关联任务'
                }
                detail_data[key]['issuelinks'].append(issue_obj)
            elif 'inwardIssue' in issue:
                issue_obj = {
                    "key": issue['inwardIssue']['key'],
                    "summary": issue['inwardIssue']['fields']["summary"],
                    "issuetype": issue['inwardIssue']['fields']["issuetype"]["name"],
                    "linktype": '被关联'
                }
                detail_data[key]['issuelinks'].append(issue_obj)

        # 评论
        comments = detail_res['fields']['comment']['comments']
        for comment in comments:
            comment_obj = {
                'author': comment['author']['displayName'],
                'body': comment['body'],
                'time': comment['created']
            }
            detail_data[key]['comments'].append(comment_obj)
        
        # 记录关键状态变更时间
        change_log = detail_res['changelog']['histories']
        respondent_time = ""
        test_start_time = ""
        resolve_time = ""
        closed_time = ""
        for one_action in change_log:
            change_time = one_action["created"]
            for item in one_action['items']:
                # bug响应为进入开发阶段
                if item['toString'] == "开发" and (detail_res['fields']['issuetype']['name']) == 'sub-Bug':
                    respondent_time = change_time
                if item['toString'] == "测试中":
                    test_start_time = change_time
                if item['toString'] == '发布上线中':
                    resolve_time = change_time
                if item['toString'] == 'Closed':
                    closed_time = change_time
        detail_data[key]['respondent_time'] = respondent_time
        detail_data[key]['test_start_time'] = test_start_time
        detail_data[key]['resolve_time'] = resolve_time or closed_time
        index += 1
        time.sleep(1)
    
    dataFileName = version + '_detail'
    print('数据采集完毕，导出数据至 ' + dataFileName + '.json')
    deletefile('./' + dataFileName + '.json')
    saveData2JSON(detail_data, dataFileName)
    print('数据导出完毕，开始统计数据')
    return detail_data


def saveData2JSON(datas, name):
    fl=open('./'+ name + '.json', 'w', encoding='UTF-8')
    fl.write(json.dumps(datas,ensure_ascii=False,indent=2))
    fl.close()

def stata(detail_data):
    person_list = ['irises']
    personal_data_obj = {}
    bug_product_obj = {}
    for person in person_list:
        personal_data_obj[person] = {
            'demand_num': 0,
            'demand_list': [],
            'history_bug_num': 0,
            'history_bug_list': [],
            'new_bug_num': 0,
            'new_bug_list': [],
            'total_workload': 0,
            'bug_workload': 0,
            'bug_ratio': 0,
            'bug_workload_ratio': 0,
            'has_deferred_test': False,
            'deferred_test_issues': [],
            'has_deferred_publish': False,
            'deferred_publish_issues': [],
            'has_deferred_close_plan': False,
            'deferred_close_issues_plan': [],
            'has_deferred_close_actual': False,
            'deferred_close_issues_actual': [],
            'unseasonal_bug_num': 0,
            'unseasonal_bug_issues': []
        }
    issuelist = list(detail_data.keys())
    for key in detail_data:
        issue = detail_data[key]
        issue['key'] = key
        assignee = issue['assignee']
        if assignee not in person_list:
            continue
        workload = issue['workload']
        create_time = issue['create_time'].split('T')[0]

        # 确定类型与统计工作量
        if issue['issuetype'] in ['新需求', '改进', 'sub-Task', '任务']:
            personal_data_obj[assignee]['demand_list'].append(issue)
            personal_data_obj[assignee]['total_workload'] += workload
        elif issue['issuetype'] == 'sub-Bug' and issue['parent']['key'] in issuelist and create_time > version_start_date:
            personal_data_obj[assignee]['bug_workload'] += workload
            personal_data_obj[assignee]['new_bug_list'].append(issue)
        else:
            personal_data_obj[assignee]['history_bug_list'].append(issue)
            personal_data_obj[assignee]['total_workload'] += workload
        
        # 计算是否按时提测
        test_start_time = issue['test_start_time'].split('T')[0]
        if test_start_time != '' and test_start_time > version_test_date and create_time < version_test_date:
            personal_data_obj[assignee]['has_deferred_test'] = True
            personal_data_obj[assignee]['deferred_test_issues'].append(issue)
        
        # 计算是否延期与是否按时封板
        resolve_time = issue['resolve_time'].split('T')[0]
        if resolve_time == '' or resolve_time > version_plan_publish_date:
            personal_data_obj[assignee]['has_deferred_publish'] = True
            personal_data_obj[assignee]['deferred_publish_issues'].append(issue)
        if resolve_time == '' or resolve_time > version_plan_close_date:
            personal_data_obj[assignee]['has_deferred_close_plan'] = True
            personal_data_obj[assignee]['deferred_close_issues_plan'].append(issue)
        # if resolve_time == '' or resolve_time > version_actual_close_date:
        #     personal_data_obj[assignee]['has_deferred_close_actual'] = True
        #     personal_data_obj[assignee]['deferred_close_issues_actual'].append(issue)

        # 计算bug是否及时响应
        respondent_time = issue['respondent_time'].split('T')[0]
        if respondent_time and respondent_time != create_time:
            personal_data_obj[assignee]['unseasonal_bug_num'] += 1
            personal_data_obj[assignee]['unseasonal_bug_issues'].append(issue)

        
        # 统计bug新增
        if issue['issuetype'] == 'sub-Bug' and issue['parent']['key'] in issuelist and create_time > version_start_date:
            if (create_time in bug_product_obj):
                bug_product_obj[create_time].append(issue)
            else:
                bug_product_obj[create_time] = [issue]

    # 统计各类型任务数量与bug率
    for persion in personal_data_obj:
        personal_data_obj[persion]['demand_num'] = len(personal_data_obj[persion]['demand_list'])
        personal_data_obj[persion]['history_bug_num'] = len(personal_data_obj[persion]['history_bug_list'])
        personal_data_obj[persion]['new_bug_num'] = len(personal_data_obj[persion]['new_bug_list'])
        personal_data_obj[persion]['bug_ratio'] = personal_data_obj[persion]['new_bug_num'] / (personal_data_obj[persion]['total_workload'] / (8 * 60 * 60)) if personal_data_obj[persion]['new_bug_num'] else 0
        personal_data_obj[persion]['bug_workload_ratio'] = personal_data_obj[persion]['bug_workload'] / personal_data_obj[persion]['total_workload'] if personal_data_obj[persion]['new_bug_num'] else 0

    # print(bug_product_obj)
    # print(personal_data_obj)
    personDataFileName = version + '_person'
    deletefile('./' + personDataFileName + '.json')
    saveData2JSON(personal_data_obj, personDataFileName)
    print('个人统计数据已保存至', personDataFileName)

    bugDataFileName = version + '_bug'
    deletefile('./' + bugDataFileName + '.json')
    saveData2JSON(bug_product_obj, bugDataFileName)
    print('bug新增数据已保存至', bugDataFileName)
    return personal_data_obj, bug_product_obj

def drawNewBugTrend(bug_product_obj):
    date_list = []
    bug_count_list = []
    for date in bug_product_obj:
        [year, month, day] = date.split('-')
        date_list.append(month + '-' + day)
        bug_count_list.append(len(bug_product_obj[date]))
    plt.figure(figsize=(12.8, 7.2))
    plt.scatter(date_list, bug_count_list, c='red', s=100, label='legend')
    plt.xlabel("日期", fontdict={'size': 12})
    plt.ylabel("Bug数", fontdict={'size': 16})
    plt.title("日新增bug统计图", fontdict={'size': 20})
    plt.grid(True)
    plt.legend(['bug'],loc='best')
    plt.rcParams['font.sans-serif'] = ['SimHei']
    deletefile('./new_bug_image.jpg')
    plt.savefig('new_bug_image.jpg', dpi=1000)
    # plt.show()

def deletefile(path):
    if(os.path.isfile(path)):
        os.remove(path)

def saveData2Excel(detail_data, personal_data_obj, bug_product_obj):
    bookname = './统计数据-' + version + '.xls'
    deletefile(bookname)
    book = xlwt.Workbook(encoding='utf-8',style_compression=0)
    
    # 统计任务详细数据
    sheet1 = book.add_sheet('版本任务数据',cell_overwrite_ok=True)
    version_issue_cols = ('任务号', '任务标题', '任务类型', '经办人', '报告人', '关联任务', '创建时间', '响应时间', '提测时间', '解决时间', '评论', '工作量')
    for i in range(len(version_issue_cols)):
        sheet1.write(0,i,version_issue_cols[i])
    version_issue_row = 1
    for key,issue in detail_data.items():
        sheet1.write(version_issue_row,0, key)
        sheet1.write(version_issue_row,1, issue['summary'])
        sheet1.write(version_issue_row,2, issue['issuetype'])
        sheet1.write(version_issue_row,3, issue['assignee'])
        sheet1.write(version_issue_row,4, issue['reporter'])
        # 关联任务
        issuelinksText = ''
        for link_issue in issue['issuelinks']:
            issuelinksText += '{0}:{1}-{2}\n'.format(link_issue['linktype'],link_issue['key'],link_issue['summary'])
        sheet1.write(version_issue_row,5, issuelinksText)
        sheet1.write(version_issue_row,6, issue['create_time'])
        sheet1.write(version_issue_row,7, issue['respondent_time'])
        sheet1.write(version_issue_row,8, issue['test_start_time'])
        sheet1.write(version_issue_row,9, issue['resolve_time'])
        # 评论
        commentText = ''
        for comment in issue['comments']:
            commentText += '{0}  {1}: {2}\n\n'.format(comment['time'].split('T')[0],comment['author'],comment['body'])
        sheet1.write(version_issue_row,10, commentText)
        sheet1.write(version_issue_row,11, str(round(issue['workload'] / 8 / 60 / 60, 2)) + 'd')
        version_issue_row += 1

    
    # 个人数据统计
    sheet2 = book.add_sheet('个人统计数据',cell_overwrite_ok=True)
    version_personal_cols = ('统计人员', '需求', '需求列表', '遗留bug', '遗留bug列表', '新增bug', '新增bug列表','新增bug工作量', 'bug率', '总工作量', '新增bug工作比率', '按时提测', 
    '提测延期原因','按时封板(预期)', '封板延期原因(预期)', '按时发布', '延期发布原因', '未及时响应bug数', '未及时响应bug列表')
    version_demand_count = 0
    version_history_bug_count = 0
    version_new_bug_count = 0
    version_new_bug_workload = 0
    version_workload = 0
    version_unseasonal_bug_count = 0
    for i in range(len(version_personal_cols)):
        sheet2.write(0,i,version_personal_cols[i])
    version_person_row = 1
    for key,person in personal_data_obj.items():
        # 人名
        sheet2.write(version_person_row,0, key)
        # 需求数
        sheet2.write(version_person_row,1, person['demand_num'])
        # 需求列表
        demandIssuesText = ''
        for issue in person['demand_list']:
            demandIssuesText += '{0}  {1}\n'.format(issue['key'], issue['summary'])
        sheet2.write(version_person_row,2, demandIssuesText)
        # 历史bug数
        sheet2.write(version_person_row,3, person['history_bug_num'])
        # 历史bug列表
        historyBugText = ''
        for issue in person['history_bug_list']:
            historyBugText += '{0}  {1}\n'.format(issue['key'], issue['summary'])
        sheet2.write(version_person_row,4, historyBugText)
        # 新增bug数
        sheet2.write(version_person_row,5, person['new_bug_num'])
        # 新增bug列表
        newBugText = ''
        for issue in person['new_bug_list']:
            newBugText += '{0}  {1}\n'.format(issue['key'], issue['summary'])
        sheet2.write(version_person_row,6, newBugText)
        # 新增bug工作量
        sheet2.write(version_person_row,7, str(round(person['bug_workload'] / 8 / 60 / 60, 2)) + 'd')
        # bug率
        sheet2.write(version_person_row,8, round(person['bug_ratio'], 2))
        # 总工作量
        sheet2.write(version_person_row,9, str(round(person['total_workload'] / 8 / 60 / 60, 2)) + 'd')
        # 新增bug工作量比率
        sheet2.write(version_person_row,10, round(person['bug_workload_ratio'], 2))

        # 是否提测延期
        sheet2.write(version_person_row,11, '否' if person['has_deferred_test'] else '是')
        # 提测延期原因
        deffer_test_reasion = ''
        for issue in person['deferred_test_issues']:
            deffer_test_reasion += '提测时间:{0}  提测任务:{1}  {2}\n'.format(issue['test_start_time'].split('T')[0], issue['key'], issue['summary'])
        sheet2.write(version_person_row,12, deffer_test_reasion)

        # 是否封板延期
        sheet2.write(version_person_row,13, '否' if person['has_deferred_close_plan'] else '是')
        # 封板延期原因
        deffer_publish_reasion = ''
        for issue in person['deferred_close_issues_plan']:
            deffer_publish_reasion += '解决时间:{0}  任务:{1}  {2}\n'.format(issue['resolve_time'].split('T')[0], issue['key'], issue['summary'])
        sheet2.write(version_person_row,14, deffer_publish_reasion)

        # 是否发布延期
        sheet2.write(version_person_row,15, '否' if person['has_deferred_publish'] else '是')
        # 发布延期原因
        deffer_publish_reasion = ''
        for issue in person['deferred_publish_issues']:
            deffer_publish_reasion += '解决时间:{0}  任务:{1}  {2}\n'.format(issue['resolve_time'].split('T')[0], issue['key'], issue['summary'])
        sheet2.write(version_person_row,16, deffer_publish_reasion)

        # 未及时响应bug数
        sheet2.write(version_person_row,17, person['unseasonal_bug_num'])
        # 未及时响应bug列表
        respondentText = ''
        for issue in person['unseasonal_bug_issues']:
            respondentText += '创建时间:{0} 响应时间：{1} 任务:{2}  {3}\n'.format(issue['create_time'].split('T')[0], issue['respondent_time'].split('T')[0], issue['key'], issue['summary'])
        sheet2.write(version_person_row,18, respondentText)
        version_person_row += 1

        version_demand_count += person['demand_num']
        version_history_bug_count += person['history_bug_num']
        version_new_bug_count += person['new_bug_num']
        version_workload += person['total_workload']
        version_new_bug_workload += person['bug_workload']
        version_unseasonal_bug_count += person['unseasonal_bug_num']
    # 写入合计数
    sheet2.write(version_person_row,0, '合计数')
    sheet2.write(version_person_row,1, version_demand_count) # 需求总数
    sheet2.write(version_person_row,3, version_history_bug_count) # 历史bug总数
    sheet2.write(version_person_row,5, version_new_bug_count) # 新增bug总数
    sheet2.write(version_person_row,7, str(round(version_new_bug_workload / 8 / 60 /60, 2)) + 'd') # 新增bug工作总量
    sheet2.write(version_person_row,8, str(round(version_new_bug_count / (version_workload / 8 / 60 /60), 2))) # 新增bug比率
    sheet2.write(version_person_row,9, str(round(version_workload / 8 / 60 /60, 2)) + 'd') # 工作总量
    sheet2.write(version_person_row,17, version_unseasonal_bug_count) # 未及时响应bug数

    
    # 新增bug数据统计
    sheet3 = book.add_sheet('新增bug统计数据',cell_overwrite_ok=True)
    version_bug_cols = ('日期', 'bug数', 'bug列表')
    for i in range(len(version_bug_cols)):
        sheet3.write(0,i,version_bug_cols[i])
    version_issue_row = 1
    for date,issues in bug_product_obj.items():
        sheet3.write(version_issue_row, 0, date)
        sheet3.write(version_issue_row, 1, len(issues))
        for issue in issues:
            sheet3.write(version_issue_row, 2, '{0}  {1}'.format(issue['key'], issue['summary']))
            version_issue_row += 1
    
    book.save(bookname)
    print('所有数据已保存至' + bookname)


version = input('请输入统计版本, 如V8.15.0.0908\n')
version_start_date = input('请输入版本开始日期，如2022-08-23\n')
version_test_date = input('请输入版本提测日期\n')
version_plan_close_date = input('请输入版本预期封板日期\n')
version_plan_publish_date = input('请输入版本预期发布日期\n')

# version = 'V8.14.0'
# version_start_date = '2022-07-20'
# version_test_date = '2022-08-08'
# version_plan_close_date = '2022-08-15'
# version_plan_publish_date = '2022-08-16'
PREFIXES = 'http://172.20.200.191:8080'

print('\n当前统计版本:{0}\n当前版本开始日期:{1}\n当前版本提测日期:{2}\n当前版本预期封板日期:{3}\n当前版本预期发布日期:{4}\n'.format(version, version_start_date, version_test_date, version_plan_close_date,version_plan_publish_date))

is_get_data_online = input('请选择使用线上源数据,(y: 线上数据 n:本地数据)\n')
while is_get_data_online != 'y' and is_get_data_online != 'n':
    is_get_data_online = input('输入不正确,请重新输入,(y: 线上数据 n:本地数据)\n')

detail_data = {}
if is_get_data_online == 'y':
    detail_data = getData()
else:
    with open('./'+ version +'_detail.json', "r", encoding='UTF-8') as f:
        detail_data = json.load(f)
personal_data_obj, bug_product_obj = stata(detail_data)
drawNewBugTrend(bug_product_obj)
saveData2Excel(detail_data, personal_data_obj, bug_product_obj)

input('程序运行完毕，请核验文件数据')

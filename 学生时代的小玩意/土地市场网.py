import requests
from selenium import webdriver
import random
import time
from lxml import etree
import re
import openpyxl
import xlsxwriter
from multiprocessing import Pool

def getheader(cookie):
    agents = [
        "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:17.0; Baiduspider-ads) Gecko/17.0 Firefox/17.0",
        "Mozilla/5.0 (Linux; U; Android 2.3.6; en-us; Nexus S Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Avant Browser/1.2.789rel1 (http://www.avantbrowser.com)",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5",
        "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.310.0 Safari/532.9",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.601.0 Safari/534.14",
        "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.27 (KHTML, like Gecko) Chrome/12.0.712.0 Safari/534.27",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.24 Safari/535.1",
        "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0 x64; en-US; rv:1.9pre) Gecko/2008072421 Minefield/3.0.2pre",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9b4) Gecko/2008030317 Firefox/3.0b4",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-GB; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)",
        "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 GTB5",
        "Mozilla/5.0 (Windows; U; Windows NT 5.1; tr; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 ( .NET CLR 3.5.30729; .NET4.0E)",
        "Mozilla/5.0 (Windows; U; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; BIDUBrowser 7.6)",
        "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko",
        "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
        "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36",
        "Mozilla/5.0 (Windows NT 6.3; Win64; x64; Trident/7.0; Touch; LCJB; rv:11.0) like Gecko",
        "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
        "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
        "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0a2) Gecko/20110622 Firefox/6.0a2",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1",
        "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b4pre) Gecko/20100815 Minefield/4.0b4pre",
        "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0 )",
        "Mozilla/4.0 (compatible; MSIE 5.5; Windows 98; Win 9x 4.90)",
        "Mozilla/5.0 (Windows; U; Windows XP) Gecko MultiZilla/1.6.1.0a",
        "Mozilla/2.02E (Win95; U)",
        "Mozilla/3.01Gold (Win95; I)",
        "Mozilla/4.8 [en] (Windows NT 5.1; U)",
        "Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.4) Gecko Netscape/7.1 (ax)",
        "HTC_Dream Mozilla/5.0 (Linux; U; Android 1.5; en-ca; Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (hp-tablet; Linux; hpwOS/3.0.2; U; de-DE) AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/234.40.1 Safari/534.6 TouchPad/1.0",
        "Mozilla/5.0 (Linux; U; Android 1.5; en-us; sdk Build/CUPCAKE) AppleWebkit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 2.1; en-us; Nexus One Build/ERD62) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 1.5; en-us; htc_bahamas Build/CRB17) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 2.1-update1; de-de; HTC Desire 1.19.161.5 Build/ERE27) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Sprint APA9292KT Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 1.5; de-ch; HTC Hero Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-us; ADR6300 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 2.1; en-us; HTC Legend Build/cupcake) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 1.5; de-de; HTC Magic Build/PLAT-RC33) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1 FirePHP/0.3",
        "Mozilla/5.0 (Linux; U; Android 1.6; en-us; HTC_TATTOO_A3288 Build/DRC79) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 1.0; en-us; dream) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
        "Mozilla/5.0 (Linux; U; Android 1.5; en-us; T-Mobile G1 Build/CRB43) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari 525.20.1",
        "Mozilla/5.0 (Linux; U; Android 1.5; en-gb; T-Mobile_G2_Touch Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Droid Build/FRG22D) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Milestone Build/ SHOLS_U2_01.03.1) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.0.1; de-de; Milestone Build/SHOLS_U2_01.14.0) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
        "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522  (KHTML, like Gecko) Safari/419.3",
        "Mozilla/5.0 (Linux; U; Android 1.1; en-gb; dream) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
        "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.1; en-us; Nexus One Build/ERD62) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Sprint APA9292KT Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-us; ADR6300 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 2.2; en-ca; GT-P1000M Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
        "Mozilla/5.0 (Linux; U; Android 3.0.1; fr-fr; A500 Build/HRI66) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
        "Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
        "Mozilla/5.0 (Linux; U; Android 1.6; es-es; SonyEricssonX10i Build/R1FA016) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
        "Mozilla/5.0 (Linux; U; Android 1.6; en-us; SonyEricssonX10i Build/R1AA056) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1"
    ]
    header = {
        # ip代理池获取时用的url的headers
        'User-Agent': random.choice(agents),
        'Cookie': cookie
    }
    return header


def getcookie(url):
    driver = webdriver.Chrome()
    driver.get(url)
    time.sleep(10)  # 等待cookie加载完成
    cookies = driver.get_cookies()
    driver.close()
    cookie = ''
    for item in cookies:
        if item != cookies[-1]:
            cookie = cookie + item['name'] + '=' + item['value'] + '; '
        else:
            cookie = cookie + item['name'] + '=' + item['value']
    return cookie

def get_page_num(date,cookie):
    header = getheader(cookie)
    paras = {
        'TAB_QueryConditionItem': '9f2c3acd-0256-4da2-a659-6949c4671a2a',
        'TAB_QuerySubmitConditionData': '9f2c3acd-0256-4da2-a659-6949c4671a2a:{}'.format(date)
    }
    url = 'https://www.landchina.com/default.aspx?tabid=263&ComName=default'
    r = requests.get(url, headers=header, data=paras)
    return re.findall('共(\d+)页', r.text)[0]

def get_list(list):
    title = ['行政区:', '电子监管号：', '项目名称:', '项目位置:', '面积(公顷):', '土地来源:', '土地用途:', '供地方式:',
             '土地使用年限:', '行业分类:', '土地级别:', '成交价格(万元):', '分期支付约定:', '支付期号', '约定支付日期', '约定支付金额(万元)'
        , '备注', '土地使用权人:', '约定容积率:', '下限:', '上限:', '约定交地时间:', '约定开工时间:'
        , '约定竣工时间:', '实际开工时间:', '实际竣工时间:', '批准单位:', '合同签订日期:']

    if list[list.index('土地来源:') + 1] == list[list.index('面积(公顷):') + 1]:
        list[list.index('土地来源:') + 1] = '现有建设用地'
    elif list[list.index('土地来源:') + 1]:
        list[list.index('土地来源:') + 1] = '新增建设用地'
    else:
        list[list.index('土地来源:') + 1] = '新增建设用地(来自存量库)'

    result = []

    for i in range(12):
        if list.index(title[i + 1]) - list.index(title[i]) == 2:
            result.append(list[list.index(title[i]) + 1])
        else:
            result.append('空')

    tip = list.index('备注')
    owner = list.index('土地使用权人:')
    if tip + 1 != owner:
        p = []
        t = list[tip + 1:owner]
        for i in range(4):
            if str(i + 1) in list[tip + 1:owner]:
                v = t.index(str(i + 1))
                t[v] = '期号:' + t[v]
                t[v + 1] = '约定支付日期:' + t[v + 1]
                t[v + 2] = '约定支付金额(万元):' + t[v + 2]
                if t[v + 3] in [str(i) for i in range(4)]:
                    t[v + 3] = '备注:空'
                else:
                    t[v + 3] = '备注:' + t[v + 3]
                p.append([t[v], t[v + 1], t[v + 2], t[v + 3]])
        result.append(str(p))
    else:
        result.append('空')

    if list[list.index('土地使用权人:') + 1] != '约定容积率:':
        result.append(list[list.index('土地使用权人:') + 1])
    else:
        result.append('空')

    lower_limit = title.index('下限:')
    i = lower_limit
    for i in range(lower_limit, len(title) - 1):
        if list.index(title[i + 1]) - list.index(title[i]) == 2 and list[list.index(title[i]) + 1] != '\xa0':
            result.append(list[list.index(title[i]) + 1])
        else:
            result.append('空')

    last = list.index('合同签订日期:')
    try:
        result.append(list[last + 1])
    except:
        result.append('空')

    return result

def get_info(url,cookie):
    try:
        # header = {
        #     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36',
        #     'Cookie': cookie
        # }
        header = getheader(cookie)

        r = requests.get(url, headers=header)
        selector = etree.HTML(r.text)
        ss = selector.xpath(
            '//table[@id="mainModuleContainer_1855_1856_ctl00_ctl00_p1_f1"]/tbody//tr[starts-with(@id,"mainModuleContainer")]')
        l = ss[0].xpath('//td/span/text()')[2:]
        return get_list(l)
    except:
        pass

def getallinfo(date):
    title = ['行政区', '电子监管号', '项目名称', '项目位置',
             '面积(公顷)', '土地来源', '土地用途', '供地方式',
             '土地使用年限', '行业分类', '土地级别', '成交价格(万元)',
             '分期支付约定:支付期号、约定支付日期、约定支付金额(万元)、备注',
             '土地使用权人', '约定容积率-下限', '约定容积率-上限',
             '约定交地时间', '约定开工时间', '约定竣工时间',
             '实际开工时间', '实际竣工时间', '批准单位', '合同签订日期']
    book = xlsxwriter.Workbook('D:/搜索引擎/爬虫/数据/2014/{}数据.xlsx'.format(date))
    sheet = book.add_worksheet()
    for h in range(len(title)):
        sheet.write(0, h, title[h])
    book.close()
    workbook = openpyxl.load_workbook('D:/搜索引擎/爬虫/数据/2014/{}数据.xlsx'.format(date))
    worksheet = workbook.active
    row = 2
    day1 = date.split('~')[0]
    day2 = date.split('~')[1]
    datetime = '2015-{}~2015-{}'.format(day1,day2)

    url = 'https://www.landchina.com/default.aspx?tabid=263&ComName=default'
    # cookie = getcookie(url)                 ##在该日期的爬虫下获取cookie
    # cookiepool = ['security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869197'
    #               ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869380'
    #                ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869436'
    #                 ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869466'
    #               ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869484'
    #                ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869512'
    #               ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869539'
    #               ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869565'
    #               ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869580'
    #               ,'security_session_mid_verify=002f2924b34acf5731f61ed1dbd90b01; security_session_verify=5b3d0cfd5d3c79641a6d50f0a00b2ed0; security_session_high_verify=8b8d5458a9ae987486155ddb35479a49; ASP.NET_SessionId=4jqysxkoilqtic5qp4qai02d; Hm_lvt_83853859c7247c5b03b527894622d3fa=1572400951,1572401599,1572607962,1572869194; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1572869620'
    #           ]
    # cookie = random.choice(cookiepool)
    cookie = 'security_session_verify=6a5636251af5aac48929eebd67f9b964; security_session_high_verify=1c747b55d831f5662d2875ab3563dd7e; Hm_lvt_83853859c7247c5b03b527894622d3fa=1575298528,1575338091,1575368747,1575380910; ASP.NET_SessionId=aebhbfb3pzazksccy11nhxcq; Hm_lpvt_83853859c7247c5b03b527894622d3fa=1575380913'
    page = get_page_num(datetime,cookie)    ##获取该日期下需爬取的页数
    for i in range(int(page)):
        header = getheader(cookie)
        paras = {
            'TAB_QuerySubmitPagerData': i + 1,
            'TAB_QueryConditionItem': '9f2c3acd-0256-4da2-a659-6949c4671a2a',
            'TAB_QuerySubmitConditionData': '9f2c3acd-0256-4da2-a659-6949c4671a2a:{}'.format(datetime)
        }
        r = requests.get(url, headers=header, data=paras)      ##获取该段日期下的指定页信息
        selector = etree.HTML(r.text)
        ss = selector.xpath('//table[@id="TAB_contentTable"]/tbody//tr[@onmouseout="this.className=rowClass"]')
        for s in ss:
            href = "https://www.landchina.com/" + s.xpath('td[@class="queryCellBordy"]/a/@href')[0]
            info = get_info(href,cookie)
            try:
                j = 1
                for data in info:
                    worksheet.cell(row, j).value = str(data)
                    j += 1
                row += 1
                workbook.save('D:/搜索引擎/爬虫/数据/2014/{}数据.xlsx'.format(date))
                print('%s的第%d页'%(date,i))
                # time.sleep(2)
            except:
                pass


if __name__ =='__main__':
    # dates = ['{}-{}~{}-{}'.format(i, j , i, j+4 ) for i in range(9, 10) for j in range(6,30,5)]  ##获取9 6 - 9 30数据

    dates1 = ['{}-{}~{}-{}'.format(i, j, i, j + 4) for i in range(1,2) for j in range(1, 30, 5)]  ##测试
    dates2 = ['2-1~2-5','2-6~2-10','2-11~2-15','2-16~2-20','2-21~2-25','2-26~2-28']
    dates3 = ['{}-{}~{}-{}'.format(i, j, i, j + 4) for i in range(3,7) for j in range(1, 30, 5)]
    dates = dates1+dates3
    # dates=dates1+dates2+dates3
    # print(dates)
    # dates = ['8-6~8-10','8-21~8-25','8-26~8-30']
    pool = Pool(processes=6)
    pool.map(getallinfo, dates)




import requests
from bs4 import BeautifulSoup
import re

headers={
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'
}

url = "https://movie.douban.com/top250"

response = requests.get(url=url, headers=headers)
# print(response.text)
# bs4解析获取值
# soup = BeautifulSoup(response.text,'html.parser')
# all_info = soup.find(name='ol',class_='grid_view')
# title_list = all_info.find_all('div',class_='item')
# for li in title_list:
#     title = li.find('span',attrs={'class':'title'}).text
#     print(title)

# 正则表达式获取
title = re.findall('<span class="title">([\u4e00-\u9fa5]{2,10})</span>',response.text)
print(title)


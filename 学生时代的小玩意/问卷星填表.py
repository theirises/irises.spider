import requests
import urllib.parse
import urllib.request
import js2py
import re
import random
import time

requests.packages.urllib3.disable_warnings()

def GetMain(url,name,sno1):
    r = requests.get(url,verify=False)
    dic_num = {}
    dic_value = {}
    dic_num['name'] = re.findall("id='divTitle(\d)' class='div_title_question'>.{0,10}[名]", r.text)[0]
    dic_num['major'] = re.findall("(\d+)\_(\d+)'>.{0,5}[信息管理]", r.text)[0][0]
    dic_value['major'] = re.findall("(\d+)\_(\d+)'>.{0,5}[信息管理]", r.text)[0][1]
    dic_num['grade'] = re.findall("(\d+)\_(\d+)'>\d+[17]", r.text)[0][0]
    dic_value['grade'] = re.findall("(\d+)\_(\d+)'>\d+[17]", r.text)[0][1]
    sno = re.findall("id='divTitle(\d)' class='div_title_question'>.{0,5}学号", r.text)
    if sno != []:
        dic_num['sno'] = sno[0]

    dic = [v[0] for v in sorted(dic_num.items(), key=lambda p: p[1], reverse=False)]

    str1 = ''
    for i in dic:
        if i in dic_value.keys():
            str1 += dic_num[i] + '$' + dic_value[i] + '}'
        elif i == 'name':
            str1 += dic_num[i] + '$'+ name+ '}'
        elif i == 'sno':
            str1 += dic_num[i] + '$' + sno1 + '}'
    str1 = str1[:str1.index('}', -1)]
    # ktimes = random.randint(30, 200)
    ktimes = 50
    ktimes = str(ktimes)
    curID = url.split("/")
    curID = curID[4][:-5]
    rn = re.findall('rndnum="(.*?)"',r.text)[0]
    jqnonce = re.findall('jqnonce="(.*?)"',r.text)[0]
    # raw_t = round(time.time(), 3)
    # t = int(str(raw_t).replace('.', ''))
    t = 1575278604143
    now = int(time.time())
    timeStruct = time.localtime(now)
    strTime = time.strftime("%Y-%m-%d %H:%M:%S", timeStruct)
    timeStruct = time.strptime(strTime, "%Y-%m-%d %H:%M:%S")
    time1 = time.strftime("%Y/%m/%d %H:%M:%S",timeStruct)
    time1 = urllib.parse.urlencode({'name':time1})[5:]
    js = '''function dataenc(a) {
    var c, d, e, b = ktimes % 10;
    for (0 == b && (b = 1), c = [], d = 0; d < a.length; d++) e = a.charCodeAt(d) ^ b,
    c.push(String.fromCharCode(e));
    return c.join("")}'''
    js = js.replace("ktimes",ktimes)
    jq = js2py.eval_js(js)
    jqsign =jq(jqnonce)
    jqsign = urllib.parse.quote(jqsign)
    url1 = "https://www.wjx.top/joinnew/processjq.ashx?submittype=1&curID="+ str(curID) +"&t="+ str(t)+"&starttime=" + str(time1)+ "&ktimes="+ str(ktimes)+"&rn="+ str(rn) +"&hlv=1&jqnonce=" + str(jqnonce) +"&jqsign="+str(jqsign)+"&jpm=15"
    return [url1,str1]

def Auto_WjX(url,data):
    headers = {
        'Host':'www.wjx.cn',
        "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36",
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Referer': 'https://www.wjx.top/jq/50841326.aspx'
    }
    data = str(data)
    data = data.encode("utf-8")
    data = urllib.parse.quote(data)
    data = "submitdata="+data
    time.sleep(7)
    r = requests.post(url, data=data, headers=headers,verify=False)
    result = r.text[0:2]
    return result

def main(url,name,sno):
    try:
        r = GetMain(url,name,sno)
        result = Auto_WjX(r[0],r[1])
        if int(result) in [10]:
            print('提交成功！！！！')
        else:
            print('提交失败！！！！')
    except:
        main(url,name,sno)

if __name__ == '__main__':
    a = True
    print("#####################################   210工作室出品  #####################################")
    print("#####################################  210出品必是精品  #####################################")
    print("\n")
    url = input("请输入形势政策课抢课网站：")
    name = input("请输入你的名字:")
    sno = input("请输入你的学号：")
    time1 = input("请输入开抢时间(例如18:00:00,注意精确到秒，冒号为英文格式)：")
    while  a == True:
        time_now = time.strftime("%H:%M:%S", time.localtime())
        if time_now == time1:
            main(url,name,sno)
            a = False

    print("抢课成功，欢迎您的使用！")
    input()






import requests
from bs4 import BeautifulSoup
import re

headers={
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'
}


def get_page(url):
    response = requests.get(url=url, headers=headers)
    # print(response.text)
    soup = BeautifulSoup(response.text,'html.parser')
    page_element = soup.select('td[id=fanye44467]')
    # print(page_element)
    page = re.findall('/(\d+)',page_element[0].text)
    page_count = page[0]
    return(page_count)

def get_all_url(url,all_href):
    href = []
    response = requests.get(url=url, headers=headers)
    # print(response.text)
    response.encoding=response.apparent_encoding
    soup = BeautifulSoup(response.text, 'html.parser')
    elements = soup.find_all(class_='c44467')
    for i in elements:
        all_href.append(i.get('href'))
        href.append(i.get('href'))
    return href

def get_all_info(url,title,body):
    response = requests.get(url=url, headers=headers)
    # print(response.text)
    response.encoding = response.apparent_encoding
    soup = BeautifulSoup(response.text, 'html.parser')
    title_elements = soup.find_all(class_='titlestyle42386')
    body_elements = soup.find_all(class_='v_news_content')
    for i in title_elements:
         title.append(i.get_text())
         print(i.get_text())
    for j in body_elements:
         body_fomat = re.sub('@(.*)}|<(.*)>','',j.get_text())
         body_fomat.replace('\n\r|\n|\r','')
         body.append(body_fomat)
         print(body_fomat)


if __name__ =='__main__':
    all_href = []
    title = []
    body = []
    url = 'http://nbubs.nbu.edu.cn/sydt/xyxw.htm'

    #获取总页数
    page = int(get_page(url))

    # #爬取第一页
    print(1)
    href = get_all_url(url, all_href)
    for j in range(0, 15, 1):
        info_url1 = href[j].replace('..', '')
        info_url2 = 'http://nbubs.nbu.edu.cn'
        info_url = info_url2 + info_url1
        get_all_info(info_url, title, body)

    # #爬取第二页到最后第二页
    url_page1 = 'http://nbubs.nbu.edu.cn/sydt/xyxw/'
    url_page2 = '.htm'
    for i in range(page-1,1,-1):
        print(i)
        url=url_page1+str(i)+url_page2
        href = get_all_url(url, all_href)
        for j in range(0,15,1):
            info_url1=href[j].replace('..','')
            info_url2='http://nbubs.nbu.edu.cn'
            info_url = info_url2+info_url1
            print(info_url)
            get_all_info(info_url, title, body)

    #爬取最后一页
    url = url_page1+'1'+url_page2
    href = get_all_url(url,all_href)
    href_count = len(href)
    for i in range(0,href_count,1):
        info_url1 = href[i].replace('..', '')
        info_url2 = 'http://nbubs.nbu.edu.cn'
        info_url = info_url2 + info_url1
        print(info_url)
        get_all_info(info_url, title, body)

